use v6;
unit grammar User::Dirs::Grammar;

token TOP { <line> + %% \n }
token line { <var-name> '=' \" <path> \" }
token var-name { \w + }
token path { '$HOME' <slash> <directory> }
token slash { "/" }
token directory { \w + }
