use v6;
unit class User::Dirs::Actions;
use System::Passwd;

our $local-user-home-dir is export = get-user-by-uid(1000).home-directory;

method TOP ($/) {
    my @paths = $<line>»<path>».made;
    my @var-name = $<line>»<var-name>».made;
    my @vars = '$' «~« @var-name;
    make %( (@vars Z @paths).flat );
}
method var-name ($/) {
    my $lowercase-var = ~$/;
    $lowercase-var .= substr(4)
                   .= subst(/'_'/, '-')
                   .= lc;
    make $lowercase-var;
}
method path ($/) {
    make "{$local-user-home-dir}/$<directory>";
}
