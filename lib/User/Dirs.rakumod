use v6;
module User::Dirs {
    use User::Dirs::Grammar;
    use User::Dirs::Actions;

    # Read in XDG_ variables
    my $file = "{$local-user-home-dir}/.config/user-dirs.dirs";
    my $lines = $file.IO
        .slurp
        .lines
        .grep(*.starts-with("XDG"))
        .join("\n");

    # Convert into variable names
    my $actions = User::Dirs::Actions.new();
    my $object = User::Dirs::Grammar.parse($lines, :actions($actions)) or die "$!";
    our %vars = $object.made;
}

# Export variables
sub EXPORT { User::Dirs::<%vars> }

=begin pod

=head1 NAME

User::Dirs - blah blah blah

=head1 SYNOPSIS

=begin code :lang<raku>

use User::Dirs;

=end code

=head1 DESCRIPTION

User::Dirs is ...

=head1 AUTHOR

Perry Thompson <contact@ryper.org>

=head1 COPYRIGHT AND LICENSE

Copyright 2020 Perry Thompson

This library is free software; you can redistribute it and/or modify it under the Artistic License 2.0.

=end pod
