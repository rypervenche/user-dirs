NAME
====

User::Dirs - Turn Bash XDG variables into scalars

SYNOPSIS
========

```raku
use User::Dirs;
```

DESCRIPTION
===========

User::Dirs is a module that reads your XDG variables in $HOME/.config/user-dirs.dirs and turns them into scalar variables for use in Raku.

AUTHOR
======

Perry Thompson <contact@ryper.org>

COPYRIGHT AND LICENSE
=====================

Copyright 2020 Perry Thompson

This library is free software; you can redistribute it and/or modify it under the Artistic License 2.0.

